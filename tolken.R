library(rvest)
library(tidyverse)

#http://campus.codeschool.com/courses/css-cross-country/level/1/section/1/frost-proof-fundamentals

# https://api3.libcal.com/api_events.php?iid=971&m=upc&cid=3819&c=&d=25858&l=50&target=_blank

libcal <- read_html("https://api3.libcal.com/api_events.php?iid=971&m=upc&cid=3819&c=&d=25858&l=50&target=_blank")



libcal %>% 
    html_nodes("table") %>% 
    .[[1]] %>% 
    html_table() %>% 
    spread(X1, X2) -> big_df  


for (i in 2:length(libcal %>% html_nodes("table"))) {
    libcal %>% 
        html_nodes("table") %>% 
        .[[i]] %>% 
        html_table() %>% 
        spread(X1, X2) -> temp_df2  
    
    big_df <- bind_rows(big_df, temp_df2)
}

reg_url <- libcal %>% 
    html_nodes("tr.s-lc-ea-treg td a") %>% 
    html_attr("href", default = NA_character_)

big_df <- bind_cols(big_df, Reg_URL = reg_url)

big_df$date <- big_df[[3]]
big_df$title <- big_df[[9]]
big_df$URL <- big_df[[10]]
(view <- paste(big_df$date,"-",big_df$title,big_df$URL))
view
